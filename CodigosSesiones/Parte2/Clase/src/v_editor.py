import tkinter as tk 
from tkinter.filedialog import askopenfile, asksaveasfilename
import sv_ttk

class Editor(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title('taller op1 - Editor de texto')
        self.rowconfigure(0,minsize=600,weight=1)
        self.columnconfigure(1,minsize=600,weight=1)
        #atributo de campo de texto
        self.campo_txt = tk.Text(self,wrap=tk.WORD)
        self.archivo = None
        self.archivo_abierto=False
        self._crear_componente()
        self._crear_menu()
        
    def _crear_menu(self):
        menu_vista = tk.Menu(self)
        self.config(menu=menu_vista)
        
        menu_archivo = tk.Menu(menu_vista,tearoff=False)
        menu_vista.add_cascade(label='Archivo',menu=menu_archivo)
        menu_archivo.add_command(label='Abrir',command=self._abrir_archivo)
        menu_archivo.add_command(label='Guardar',command=self._guardar_archivo)
        menu_archivo.add_command(label='Guardar como ...',command=self._guardar_como)
        menu_archivo.add_separator()
        menu_archivo.add_command(label='Salir',command=self.quit)
        
        
    def _crear_componente(self):
        frame_botones = tk.Frame(self,relief=tk.RAISED,bd=2)
        boton_abrir = tk.Button(frame_botones,text='Abrir',command=self._abrir_archivo)
        boton_guardar = tk.Button(frame_botones,text='Guardar',command=self._guardar_archivo)
        boton_guardar_como = tk.Button(frame_botones,text='Guardar como..',command=self._guardar_como)
        
        boton_abrir.grid(row=0,column=0,sticky='we',padx=5,pady=5)
        boton_guardar.grid(row=1,column=0,sticky='we',padx=5,pady=5)
        boton_guardar_como.grid(row=2,column=0,sticky='we',padx=5,pady=5)
        frame_botones.grid(row=0,column=0,sticky='ns')
        self.campo_txt.grid(row=0,column=1,sticky='nswe')
    
    def _guardar_archivo(self):
        
        if self.archivo_abierto:
            with open(self.archivo_abierto.name,'w') as self.archivo:
                txt = self.campo_txt.get(1.0,tk.END)
                self.archivo.write(txt)
                self.title('Taller op1 - {}'.format(self.archivo.name))
        else:
            self._guardar_como()        
    
    def _guardar_como(self):
        self.archivo = asksaveasfilename(defaultextension='txt',
                                         filetypes=[('Archivos de texto','*.txt'),
                                                    ('Cualquier Archivo','*.*')])
        if not self.archivo:
            return
        with open(self.archivo,'w') as archivo:
            txt = self.campo_txt.get(1.0,tk.END)
            archivo.write(txt)
            self.title('taller op1 - {}'.format(archivo.name))
            self.archivo_abierto= archivo
        
    def _abrir_archivo(self):
        self.archivo_abierto = askopenfile(mode='r+')
        self.campo_txt.delete(1.0,tk.END)
        if not self.archivo_abierto:
            return
        with open(self.archivo_abierto.name, 'r+') as self.archivo:
            texto = self.archivo.read()
            self.campo_txt.insert(1.0,texto)
            self.title('Taller op1 - {}'.format(self.archivo.name))
    
    
        
        
if __name__ == '__main__':
    editor = Editor()
    # sv_ttk.use_dark_theme()
    # sv_ttk.use_light_theme()
    sv_ttk.set_theme("dark")
    editor.mainloop()

