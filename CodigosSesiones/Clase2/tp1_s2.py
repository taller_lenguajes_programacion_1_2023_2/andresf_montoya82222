# VARIABLES Y TIPOS DE VARIABLES

nombre = "Hello World"
print(type(nombre))

entero = 25
print(type(entero))

flotante = 14.3
print(type(flotante))

booleano = True
print(type(booleano))

# LISTAS

lista = [1,2,3,4,5]
print(type(lista))

lista2= lista
lista2.append(6)
print(lista2)

#DICCIONARIOS

diccionario = {
    "nombre": "Ana",
    "nombre": "Andres",
    "edad": 25}

#TUPLA
tupla = (10,20,30)

#CONJUNTO

conjunto = {1,2,3}

print(diccionario)

#---LECTURA DE ARCHIVOS PLANOS---

with open(".\CodigosSesiones\ArchivosPlanos\Prueba1.txt", "w") as documento:
    documento.write("Hola Mundo")
with open (".\CodigosSesiones\ArchivosPlanos\Prueba1.txt", "r") as documento:
    print(documento.read())
with open(".\CodigosSesiones\ArchivosPlanos\Prueba1.txt", "a") as documento:
    documento.write("\nComo vamos")
with open (".\CodigosSesiones\ArchivosPlanos\Prueba1.txt", "r") as documento:
    print(documento.read())
