from django.db import models

# Create your models here.
class Usuario(models.Model):
    idUsuario = models.IntegerField(primary_key=True)     
    nombreUsuario = models.CharField(max_length=50)     
    apellidoUsuario = models.CharField(max_length=100)         
    correoUsuario = models.CharField(max_length=50)     
    claveUsuario = models.CharField(max_length=12)