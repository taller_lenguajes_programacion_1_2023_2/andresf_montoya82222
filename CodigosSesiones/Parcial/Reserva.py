import datetime
import Cliente as Cliente
import Hotel as Hotel
import Habitacion as Habitacion
class Reserva(Habitacion):
    def __init__(self, id="", fechaEntrada=datetime, fechaSalida =datetime, cliente=Cliente, hotel=Hotel, total=0.0):
        super().__init__()
        self.id = id
        self.fechaEntrada = fechaEntrada
        self.fechaSalida = fechaSalida
        self.cliente = cliente
        self.hotel = hotel
        self.total = total

        # GETTERS
    def getId(self):
        return self.id

    def getFechaEntrada(self):
        return self.fechaEntrada

    def getFechaSalida(self):
        return self.fechaSalida

    def getCliente(self):
        return self.cliente
    
    def getHotel(self):
        return self.hotel

    def getTotal(self):
        return self.total


    # SETTERS

    def setId(self, id):
        self.id = id

    def setFechaEntrada(self, fechaEntrada):
        self.fechaEntrada = fechaEntrada

    def setFechaSalida(self, fechaSalida):
        self.fechaSalida = fechaSalida

    def setCliente(self, cliente):
        self.cliente = cliente

    def setHotel(self, hotel):
        self.hotel = hotel

    def setTotal(self, total):
        self.total = total

