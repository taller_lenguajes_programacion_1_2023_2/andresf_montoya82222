import pandas as pd

## 1. Lee una tabla HTML desde página ##
## 2. Guardar la tabla HTML en un Excel ##

tablaHTML = pd.read_html("https://imgcdn.larepublica.co/cms/2018/10/21131151/Canasta-completa-del-IVA.pdf")[1]
tablaHTML.to_excel("./CodigosSesiones\ArchivosPlanos\Clase7(Repaso)\clase_7_v1.xlsx")

## 3. Exporta los primeros diez(10) datos en un archivo CSV ##
tablaHTML.head(10).to_csv(".\CodigosSesiones\ArchivosPlanos\Clase7(Repaso)\clase_7_v1.csv")

## 4. Lee tres tablas HTML y guardalas en 3 hojas ##

tabla1 = pd.read_html("https://onepiece.fandom.com/es/wiki/Lista_de_personajes_canon")[0]
tabla2 = pd.read_html("https://www.uv.mx/pozarica/caa-conta/files/2016/02/REGULAR-AND-IRREGULAR-VERBS.pdf")[1]
tabla3 = pd.read_html("https://imgcdn.larepublica.co/cms/2018/10/21131151/Canasta-completa-del-IVA.pdf")[0]
tabla2 = tabla2.tail(5)
tabla3 = tabla3.head(5)
with pd.ExcelWriter(".\CodigosSesiones\ArchivosPlanos\Clase7(Repaso)\clase_7_v2.xlsx") as escribir:
    tabla1.to_excel(escribir,sheet_name="Hoja#1")
    tabla2.to_excel(escribir,sheet_name="Hoja#2")
    tabla3.to_excel(escribir,sheet_name="Hoja#3")

## 5. Filtra los registros donde la columna edad >= 30 ##

Doc = pd.read_excel(".\CodigosSesiones\ArchivosPlanos\Clase4\clase_4_dataset.xlsx")
filtroEdad = Doc.loc(Doc["Edad"]>=30)
filtroEdad.to_csv(".\CodigosSesiones\ArchivosPlanos\Clase7(Repaso)\clase_7_v2.csv")

## 6. Lee un archivo CSV, guardar columnas "Nombre" y "Profesion" ##
Datos = pd.read_csv(".\CodigosSesiones\ArchivosPlanos\Clase6(Repaso)\RepasoCSV.csv")

guardarColumnas = ['Nombre', 'Profesion']
Datos[guardarColumnas].to_excel(".\CodigosSesiones\ArchivosPlanos\Clase7(Repaso)\clase_7_v3.xlsx")

## 7. Combinar dos hojas de un excel en un dataframe ##
incluir = pd.ExcelFile(".\CodigosSesiones\ArchivosPlanos\Clase7(Repaso)\clase_7_v2.xlsx")
tabla1 = pd.read_excel(incluir,"Hoja#1")
tabla2 = pd.read_excel(incluir,"Hoja#2")
combinacion = pd.concat([tabla1, tabla2])
combinacion.to_excel(".\CodigosSesiones\ArchivosPlanos\Clase7(Repaso)\clase_7_v4.xlsx")

##listaDic = list(afml_diccionario.values())

##listaDic.d