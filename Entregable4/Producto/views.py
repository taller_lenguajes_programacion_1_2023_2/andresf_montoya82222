from django.shortcuts import render, redirect, get_object_or_404
from .models import Producto

# Create your views here.
def visualizarCat(request, categoria):
    array = Producto.objects.all()
    print(categoria)
    return render(request, "visualizarCat.html", {
        "categoria": categoria,
        "error": "No correcto",
        "arreglo": array
    })
def productocat(request):
    return render(request, "productocat.html")
def especificaciones(request, id):
    producto = get_object_or_404(Producto, idProducto=id)
    return render(request, 'especificaciones.html', {'producto': producto})
def regProducto(request):
    mensajeError = "No hay datos correctos"
    if request.method == 'POST':
        nombre = request.POST['nombreProducto']
        descripcion = request.POST['descripcionProducto']
        categoria = request.POST['Categoria']
        ruta1 = request.POST['rutaProducto1']
        ruta2 = request.POST['rutaProducto2']
        ruta3 = request.POST['rutaProducto3']
        precio = request.POST['precioProducto']
        try:
            productoEn = Producto.objects.get(nombreProducto=nombre)
            mensajeError = "Producto ya se encuentra registrado"
        except:
            nuevoUser = Producto.objects.create(
                nombreProducto=nombre,
                descripcionProducto=descripcion,
                categoriaProducto=categoria,
                ruta1=ruta1,
                ruta2=ruta2,
                ruta3=ruta3,
                precioProducto=precio
            )
            return redirect('regProducto')
        
    return render(request, "regProducto.html", {
        "error": mensajeError
    })
