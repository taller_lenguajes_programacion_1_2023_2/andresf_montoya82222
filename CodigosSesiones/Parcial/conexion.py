import sqlite3
import json

class DataBase:
    def __init__(self):
        """
            Se conecta con la base de datos .sqlite y lee lo que tenga en el query.json
        """
        self.database = sqlite3.connect("/database.sqlite")
        self.pointer = self.database.cursor()    

        with open("./Query.json","r") as consultas:
            self.queries = json.load(consultas)

    def CrearTabla(self, nombreTabla="",Columnas=""):
        if nombreTabla!="" and Columnas!="":
            info = self.queries["CrearTabla"].format(nombreTabla,self.queries[Columnas])
            self.pointer.execute(info)
            self.database.commit()
            print(f"Base de datos, tabla {nombreTabla} se ha creado correctamente.")
    
    def InsertarDato(self,tabla="",interrogantes="",valores=""): #Crear
        if tabla!="" and valores!="":
            info = self.queries["InsertarDato"].format(tabla,interrogantes)
            self.pointer.executemany(info,valores)
            self.database.commit()

    def LeerDato(self, tabla = "", id=""):
        if tabla!= "" and id!="":
            info = self.queries["LeerDato"].format(tabla,id)
            self.pointer.execute(info)
            self.database.commit()
            return self.pointer.fetchone()

    def ActualizarDato(self, tabla = "", datoAct = "", datoCambio = "", id = ""):
        if tabla!= "" and datoAct != "" and datoCambio!="":
            info = self.queries["ActualizarDato"].format(tabla,datoAct,datoCambio,id)
            self.pointer.execute(info)
            self.database.commit()
            
    def EliminarDato(self, tabla = "", id = ""):
        if tabla!= "" and id!="":
            info = self.queries["EliminarDato"].format(tabla,id)
            self.pointer.execute(info)
            self.database.commit()