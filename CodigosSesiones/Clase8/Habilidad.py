
class Habilidad:
    def __init__(self,id=0,nom="",nivel=0) -> None:
        self.__id=id
        self.__nom=nom 
        self.__nivel = nivel

    @property
    def _id(self):
        return self.__id

    @_id.setter
    def _id(self, value):
        self.__id = value

    @property
    def _nom(self):
        return self.__nom

    @_nom.setter
    def _nom(self, value):
        self.__nom = value

    @property
    def _nivel(self):
        return self.__nivel

    @_nivel.setter
    def _nivel(self, value):
        self.__nivel = value



    
    def __str__(self) -> str:
        return f"habilidades {self.id}  {self.nom}  {self.nivel}"
