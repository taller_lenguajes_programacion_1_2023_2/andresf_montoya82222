class Laboral:
    def __init__(self,id=0,nom_emp="", f_ini="", f_fin="",cargo="",funciones="") -> None:
        self.__id =id
        self.__nom_emp = nom_emp
        self.__f_ini = f_ini
        self.__f_fin = f_fin
        self.__cargo = cargo
        self.__funciones= funciones

    @property
    def _id(self):
        return self.__id

    @_id.setter
    def _id(self, value):
        self.__id = value

    @property
    def _nom_emp(self):
        return self.__nom_emp

    @_nom_emp.setter
    def _nom_emp(self, value):
        self.__nom_emp = value

    @property
    def _f_ini(self):
        return self.__f_ini

    @_f_ini.setter
    def _f_ini(self, value):
        self.__f_ini = value

    @property
    def _f_fin(self):
        return self.__f_fin

    @_f_fin.setter
    def _f_fin(self, value):
        self.__f_fin = value

    @property
    def _cargo(self):
        return self.__cargo

    @_cargo.setter
    def _cargo(self, value):
        self.__cargo = value

    @property
    def _funciones(self):
        return self.__funciones

    @_funciones.setter
    def _funciones(self, value):
        self.__funciones = value

    def __str__(self) -> str:
        pass
        
