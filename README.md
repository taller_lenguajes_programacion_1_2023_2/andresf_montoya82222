# Taller de Programación 1: README:
Bienvenidos al repositorio oficial del curso "Taller de Programación 1". Aquí encontrarán todos los recursos, fechas y temarios relacionados con el curso.
## Fechas de Evaluación:
1. **Entregable #1**: 7 de Septiembre, 2023
2. **Entregable #2**: 28 de Septiembre, 2023
3. **Parcial 1**: 5 Octubre, 2023
4. **Entregable #3**: 2 Noviembre, 2023
5. **Entregable #4**: 30 Noviembre, 2023
6. **Parcial Final**: 7 Diciembre, 2023
## Herramientas Utilizadas:
- **Git**: [Sitio oficial](https://git-scm.com/)
- **Visual Studio Code**: [Descargar aquí](https://code.visualstudio.com/) Extenciones (Python , Git grafh , Git lens)
- **Python 3.9.2**: [Python descarga](https://www.python.org/ftp/python/3.9.2/python-3.9.2-amd64.exe)
## Control de Versiones:
- **git clone** <url>
- **git add** .  Empaquetar cambios
- **git commit -m "descripcion del cambio"** Etiquetar version
- **git push origin main** Carga o empuja el paquete a la ubicacion remota
## Espacio de Trabajo:
- **python -m venv venv** creacion de espacio de trabajo
- **.\venv\Scripts\activate** activar entorno virtual
## Comandos VSCode:
- Control + ñ = Abre terminal en Vs
## Comandos Pip
- **Jupyter notebook** lanza una interfaz web para trabajar con cuadernos de jupyter 
# Librerias utilizadas:
- **pip install** '<lib>' (pandas, openpyxl)
- **openpyxl** - guardar los archivos en excel
- **actualizar pip** python -m pip install -U pip
- **Importar Librerias** import <nombre_libreria> as <alias_libreria>
# Comandos with open:
-   with open(".\CodigosSesiones\ArchivosPlanos\Prueba1.txt", "w") as '<doc>': (Escribir)
-   with open(".\CodigosSesiones\ArchivosPlanos\Prueba1.txt", "r") as '<doc>': (Leer)
-   with open(".\CodigosSesiones\ArchivosPlanos\Prueba1.txt", "a") as '<doc>': (Anexar)
## Sesiones de clases:
| Sesión | Fecha       | Tema                                                      |
|--------|-------------|-----------------------------------------------------------|
| 1      | 09/08/2023  | Introducción a la programación                           |
| 2      | 10/08/2023  | Control de versiones                                     |
| 3      | 16/08/2023  | Variables y Tipos de Variables, estructura de datos, lectura y escritura de archivos ".CodigosSesiones\Clase2\tp1_sesion_2.py"                                    |
| 4      | 17/08/2023  | lectura y escritura de archivos planos ".CodigosSesiones\Clase3\tp1_sesion_3py"                                   |
| 5      | 23/08/2023  | Repaso de clases anteriores                                |
| 6      | 24/08/2023  | Lista desde html                               |
| 7      | 30/08/2023  | Repaso de todo lo anterior                              |
| 8      | 06/09/2023  | Entregable 1                             |
| 9      | 13/09/2023  | Programacion POO: Clases, atributos y objetos                            |