from django.db import models

# Create your models here.
class Producto(models.Model):
    idProducto = models.IntegerField(primary_key=True)     
    nombreProducto = models.CharField(max_length=50)
    descripcionProducto = models.CharField(max_length=200)
    categoriaProducto = models.CharField(max_length=50)   
    ruta = models.CharField(max_length=100)     
    precioProducto = models.FloatField(max_length=50)