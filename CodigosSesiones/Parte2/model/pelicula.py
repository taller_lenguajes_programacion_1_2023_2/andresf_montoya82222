from .conexion import Conexion
from tkinter import messagebox

class Pelicula(Conexion):
    def __init__(self,nombre="",duracion="",genero="") :
        super().__init__()
        self.id_pelicula = None
        self.nombre = nombre
        self.duracion = duracion
        self.genero = genero
        self.conexion = Conexion()
        self.cursor = self.conexion.cursor
        
    def guardar(self):
        query = """
        INSERT INTO peliculas ( nombre, duracion ,genero )
        VALUES ('{}' , '{}' , '{}')
        """.format(self.nombre,self.duracion,self.genero)
        try:
            self.cursor.execute(query)
            self.conexion.cerrar()
        except Exception as error:
            print(error)

    def editar(self,id_pelicula=""):
        query = """
        UPDATE peliculas 
        SET nombre = '{}' , duracion = '{}' ,  genero = '{}'
        WHERE id_pelicula = {}
        """.format(self.nombre,self.duracion,self.genero,id_pelicula)

        try:
            self.cursor.execute(query)
            self.conexion.cerrar()
        except Exception as error:
            print(error)

    def eliminar(self,id_pelicula=""):
        if id_pelicula != "":
            query = """
            DELETE FROM peliculas WHERE id_pelicula = '{}'
            """.format(id_pelicula)
            try:
                self.cursor.execute(query)
                self.conexion.cerrar()
                messagebox.showinfo("INFO : Eliminacion Exitosa"," Se elimino correctamente el registro {}".format(id_pelicula))
            except Exception as error:
                print(error)
                messagebox.showerror("ERROR: Eliminacion No Exitosa"," No se pudo realizar la eliminacion")
        else:
            print("error: no hay id_pelicula para eliminar")

    def listar(self):
        self.conexion = Conexion()
        query = """
        SELECT * FROM peliculas 
        """
        lista= []
        try:
            self.cursor.execute(query)
            lista = self.cursor.fetchall()
            self.conexion.cerrar()
        except Exception as error:
            print(error)
        return lista 

  
        
    
    
    def __str__(self) -> str:
        return "pelicula {} , {} , {}".format(self.nombre,self.duracion,self.genero)
