class Estudio:
    def __init__(self,id=0,nom="",f_ini="",f_fin="",horas=0) :
        self.__id = id
        self.__nom=nom
        self.__f_ini = f_ini
        self.__f_fin = f_fin
        self.__horas=horas

    @property
    def _id(self):
        return self.__id

    @_id.setter
    def _id(self, value):
        self.__id = value

    @property
    def _nom(self):
        return self.__nom

    @_nom.setter
    def _nom(self, value):
        self.__nom = value

    @property
    def _f_ini(self):
        return self.__f_ini

    @_f_ini.setter
    def _f_ini(self, value):
        self.__f_ini = value

    @property
    def _f_fin(self):
        return self.__f_fin

    @_f_fin.setter
    def _f_fin(self, value):
        self.__f_fin = value

    @property
    def _horas(self):
        return self.__horas

    @_horas.setter
    def _horas(self, value):
        self.__horas = value

        
    def __str__(self) :
        return f"Estudios {self.id} {self.nom}  {self.f_ini}  {self.f_fin} {self.horas}"
    
