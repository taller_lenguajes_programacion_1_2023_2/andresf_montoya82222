"""
URL configuration for AppPortal project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Web.views import inicio, login, registro
from Producto.views import visualizarCat, productocat, especificaciones, regProducto

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', inicio, name="Inicio"),
    path('login/', login, name="Login"),
    path('registro/', registro, name="registro"),
    path('productocat/', productocat, name="productocat"),
    path('visualizarCat/<str:categoria>/', visualizarCat, name="visualizarCat"),
    path('especificaciones/<int:id>/', especificaciones, name='especificaciones'),
    path('regProducto/', regProducto, name="regProducto"),
]
