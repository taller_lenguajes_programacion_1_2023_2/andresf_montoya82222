from django.shortcuts import render, redirect
from .models import Usuario


# Create your views here.
def inicio(request):
    return render(request, "inicio.html")

def login(request):
    mensajeError = ""
    if request.method == 'POST':
        usuario = request.POST['username']
        clave = request.POST['password']
        try:
            usuarioEn = Usuario.objects.get(usuario = usuario)
            if usuarioEn.claveUsuario == clave:
                return redirect('productocat')
            else:
                mensajeError = "Contrasena incorrecta"
        except:
            mensajeError = "Usuario no encontrado"
    return render(request, "login.html",{
        "error":mensajeError
    })

def registro(request):
    mensajeError = "No hay datos correctos"
    if request.method == 'POST':
        nombre = request.POST['nombreUser']
        apellido = request.POST['apellidoUser']
        correo = request.POST['correoUser']
        usuario = request.POST['usuarioUser']
        clave = request.POST['contasenaUser']

        try:
            usuarioEn = Usuario.objects.get(usuario = usuario)
            mensajeError = "Usuario ya se encuentra registrado"
        except:
            nuevoUser = Usuario.objects.create(nombreUsuario = nombre, apellidoUsuario = apellido, 
            usuario = usuario, correoUsuario = correo, claveUsuario = clave)
            return redirect('Login')



    return render(request, "registro.html",{
        "error": mensajeError
    })
