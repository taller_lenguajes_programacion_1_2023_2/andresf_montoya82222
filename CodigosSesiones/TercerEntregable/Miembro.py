import sqlite3

class Miembros:

    def __init__(self, id, nombre_miembro, club_id):
        super().__init__(club_id)
        self.id = id
        self.nombre_miembro = nombre_miembro
        self.club = None

    def __str__(self):
        return f'Miembro {self.id}: {self.nombre_miembro}'

    def guardar(self):
        conn = sqlite3.connect('database.sqlite3')
        cur = conn.cursor()
        cur.execute('''INSERT INTO members (name, club_id) VALUES (?, ?)''', (self.nombre_miembro, self.club_id))
        conn.commit()
        conn.close()

    def obtener_por_id(id):
        conn = sqlite3.connect('database.sqlite3')
        cur = conn.cursor()
        cur.execute('''SELECT * FROM members WHERE id = ?''', (id,))
        miembro = cur.fetchone()
        conn.close()
        return Miembros(*miembro) if miembro is not None else None

    def actualizar(self):
        conn = sqlite3.connect('database.sqlite3')
        cur = conn.cursor()
        cur.execute('''UPDATE members SET name = ?, club_id = ? WHERE id = ?''', (self.nombre_miembro, self.club_id, self.id))
        conn.commit()
        conn.close()

    def eliminar(self):
        conn = sqlite3.connect('database.sqlite3')
        cur = conn.cursor()
        cur.execute('''DELETE FROM members WHERE id = ?''', (self.id,))
        conn.commit()
        conn.close()

