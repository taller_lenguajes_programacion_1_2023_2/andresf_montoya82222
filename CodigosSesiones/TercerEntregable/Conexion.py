import sqlite3
import pandas as pd
import json

def crear_conexion():
    conn = sqlite3.connect('database.sqlite3')
    return conn

def cargar_consultas(conn):
    with open('query.json', 'r') as f:
        queries = json.load(f)

    for query in queries:
        conn.execute(query)

def main():
    conn = crear_conexion()
    cargar_consultas(conn)

if __name__ == '__main__':
    main()