from django.shortcuts import render, redirect, get_object_or_404
from .models import Producto

# Create your views here.
def regProductos(request):
    mensajeError = "No hay datos correctos"
    if request.method == 'POST':
        nombre = request.POST['Nombre']
        descripcion = request.POST['Descripcion']
        categoria = request.POST['Tipo']
        ruta = request.POST['Imagen']
        precio = request.POST['Precio']
        try:
            productoEn = Producto.objects.get(nombreProducto=nombre)
            mensajeError = "Producto ya se encuentra registrado"
        except:
            nuevoUser = Producto.objects.create(
                nombreProducto=nombre,
                descripcionProducto=descripcion,
                categoriaProducto=categoria,
                ruta=ruta,
                precioProducto=precio
            )
            return redirect('regProductos')
        
    return render(request, 'regProductos.html', {'mensajeError': mensajeError})

def visualizarProductos(request):
    productos = Producto.objects.all()
    return render(request, 'visualizar.html', {'productos': productos})

def compras(request, producto_id):
    producto_seleccionado = get_object_or_404(Producto, idProducto=producto_id)
    return render(request, 'compras.html', {'producto_seleccionado': producto_seleccionado})