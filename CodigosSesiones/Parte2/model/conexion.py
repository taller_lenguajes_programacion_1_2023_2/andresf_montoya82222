import sqlite3
from tkinter import messagebox
class Conexion:
    def __init__(self):
        self.ruta_db = 'C:\Users\prueb\OneDrive\Escritorio\Taller de Lenguajes 1\andresf_montoya82222\CodigosSesiones\Parte2\Clase\src\static\db'
        self.conexion = sqlite3.connect(self.ruta_db)
        self.cursor = self.conexion.cursor()
        #self.crear_tabla()
        
    
    def crear_tabla(self):
        query= """
        CREATE TABLE peliculas (
            id_pelicula INTEGER,
            nombre VARCHAR (100), 
            duracion VARCHAR (10),
            genero VARCHAR (100),
            PRIMARY KEY (id_pelicula AUTOINCREMENT)
        )"""
        try:
                self.cursor.execute(query)
                self.cerrar()
                messagebox.showinfo("INFO : Creación Tabla Exitosa"," Se creo la tabla correctamente ")
        except Exception as error:
                print(error)
                messagebox.showerror("ERROR: Creación Tabla No Exitosa"," No se pudo crear la tabla")

    def borrar_tabla(self):
        query= """
        DROP TABLE peliculas"""
        try:
                self.cursor.execute(query)
                self.cerrar()
                messagebox.showinfo("INFO : Borrado Tabla Exitosa"," Se elimino la tabla correctamente ")
        except Exception as error:
                print(error)
                messagebox.showerror("ERROR: Borrado Tabla No Exitosa"," No se pudo elimnar la tabla")
   
    
    def cerrar(self):
        self.conexion.commit()
        self.conexion.close()
        
