## Entregable #1- Andrés Felipe Montoya López- 1000758778 ##

## 58. Escribe un programa que convierta un archivo CSV en json ##

import pandas as pd

afml_leerdatos = pd.read_csv(".\CodigosSesiones\PrimerEntregable\Archivos\P6_Archivo1.csv")

afml_leerdatos.to_json(".\CodigosSesiones\PrimerEntregable\Archivos\P6_Archivo1.json")
