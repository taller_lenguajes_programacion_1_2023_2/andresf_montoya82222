import tkinter as tk # ventanas
from tkinter import ttk # componentes 
#pip install sv-ttk
import sv_ttk


def toggle_theme():
    if sv_ttk.get_theme() == "dark":
        print("Setting theme to light")
        sv_ttk.use_light_theme()
    elif sv_ttk.get_theme() == "light":
        print("Setting theme to dark")
        sv_ttk.use_dark_theme()
    else:
        print("Not Sun Valley theme")
   
def evento_boton1():
    # print(entrada1.get())
    entrada1.select_range(0,tk.END)
    entrada1.focus()
    print(entrada_var.set(""))
    # entrada1.select_clear()
    

    

# def evento_boton2():
#     boton2.config(text="presionado")   
    
ventana = tk.Tk()
ventana.geometry("600x400")
ventana.title("Ventana 1")
ventana.iconbitmap("D:/clases poli/taller programacion 1/ejercicios_tkinter/src/static/img/icon.ico")
#boton1 = ttk.Button(ventana,text="ejecutar",command=evento_boton1)
boton1 = ttk.Button(ventana,text="ejecutar",command=toggle_theme)
boton1.grid(row=1, column=2)
# entrada1 = ttk.Entry(ventana, width=30, justify=tk.CENTER,show="*")
entrada_var = tk.StringVar(value="valor por default")
# entrada1 = ttk.Entry(ventana, width=30, justify=tk.CENTER,state=tk.NORMAL)
entrada1 = ttk.Entry(ventana, width=30, textvariable=entrada_var)
entrada1.grid(row=0,column=1)
label1 = tk.Label(ventana,text="nombre")
label1.grid(row=0,column=0)
# entrada1.insert(0,"introduce la descripcion aqui")

# entrada1.insert(tk.END,".")
# boton2 = ttk.Button(ventana,text="ejecutar",command=evento_boton2)
# boton2.grid(row=0, column=0)


sv_ttk.use_dark_theme()
sv_ttk.use_light_theme()
# sv_ttk.set_theme("dark")
ventana.mainloop()
