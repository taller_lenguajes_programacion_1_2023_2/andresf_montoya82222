import tkinter as tk 
from tkinter import ttk , messagebox
from model.pelicula import Pelicula

class Frame(tk.Frame):
    def __init__(self,root=None):
        super().__init__(root,width=480,height=320)
        self.root = root
        self.pack()
        self.id_pelicula=None
        self.pelicula= Pelicula()
        #self.config(bg='green')# cambiar color de la pantalla
        self.campos_pelicula()
        self.desabilitar_campos()
        self.tabla_peliculas()
        
    def campos_pelicula(self):
        self.lbl_nombre = tk.Label(self,text='Nombre: ')
        self.lbl_nombre.config(font=('Arial',12,'bold'))
        self.lbl_nombre.grid(row=0,column=0,padx=10,pady=10)
        
        self.lbl_duracion = tk.Label(self,text='Duración: ')
        self.lbl_duracion.config(font=('Arial',12,'bold'))
        self.lbl_duracion.grid(row=1,column=0,padx=10,pady=10)
        
        self.lbl_genero = tk.Label(self,text='Genero: ')
        self.lbl_genero.config(font=('Arial',12,'bold'))
        self.lbl_genero.grid(row=2,column=0,padx=10,pady=10)
        
        self.var_nombre = tk.StringVar()
        self.input_nombre = tk.Entry(self,textvariable=self.var_nombre)
        self.input_nombre.config(width=50,font=('Arial',12))
        self.input_nombre.grid(row=0,column=1,padx=10,pady=10,columnspan=2)
        
        self.var_duracion = tk.StringVar()
        self.input_duracion = tk.Entry(self,textvariable=self.var_duracion)
        self.input_duracion.config(width=50,font=('Arial',12))
        self.input_duracion.grid(row=1,column=1,padx=10,pady=10,columnspan=2)
        
        self.var_genero = tk.StringVar()
        self.input_genero = tk.Entry(self,textvariable=self.var_genero)
        self.input_genero.config(width=50,font=('Arial',12))
        self.input_genero.grid(row=2,column=1,padx=10,pady=10,columnspan=2)
        
        #botones
        self.btn_nuevo = tk.Button(self,text='Nuevo',command=self.habilitar_campos)
        self.btn_nuevo.config(width=20, font=('Arial',12,'bold'))
        self.btn_nuevo.grid(row=3,column=0,padx=10,pady=10)
        
        self.btn_guardar = tk.Button(self,text='Guardar',command=self.guardar_datos)
        self.btn_guardar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
        self.btn_guardar.grid(row=3,column=1,padx=10,pady=10)
        
        self.btn_cancelar = tk.Button(self,text='Cancelar',command=self.desabilitar_campos)
        self.btn_cancelar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
        self.btn_cancelar.grid(row=3,column=2,padx=10,pady=10)
        
    def habilitar_campos(self):
        self.var_nombre.set('')
        self.var_duracion.set('')
        self.var_genero.set('')
        
        self.input_nombre.config(state='normal')
        self.input_duracion.config(state='normal')
        self.input_genero.config(state='normal')
        
        self.btn_guardar.config(state='normal')
        self.btn_cancelar.config(state='normal')
        
    
    def desabilitar_campos(self):
        self.var_nombre.set('')
        self.var_duracion.set('')
        self.var_genero.set('')
        
        self.input_nombre.config(state='disabled')
        self.input_duracion.config(state='disabled')
        self.input_genero.config(state='disabled')
        
        self.btn_guardar.config(state='disabled')
        self.btn_cancelar.config(state='disabled')
    
    def guardar_datos(self):
        
        self.pelicula = Pelicula(
            self.var_nombre.get(),
            self.var_duracion.get(),
            self.var_genero.get()
        )
        if self.id_pelicula == None:
           self.pelicula.guardar() 
        else:
           self.pelicula.editar(self.id_pelicula) 
        self.tabla_peliculas()
        self.desabilitar_campos()
    
    def tabla_peliculas(self):
        self.lista_peliculas = self.pelicula.listar()
        self.lista_peliculas.reverse()
        
        self.tabla = ttk.Treeview(self,
        columns=('Nombre', 'Duración', 'Genero'))
        self.tabla.grid(row=4,column=0, columnspan=4,sticky='nse')
        
        self.scroll = ttk.Scrollbar(self,orient='vertical',command=self.tabla.yview)
        self.scroll.grid(row=4,column=0, columnspan=4,sticky='nse')
        
        self.tabla.configure(yscrollcommand=self.scroll.set)
        self.tabla.heading('#0',text='ID')
        self.tabla.heading('#1',text='NOMBRE')
        self.tabla.heading('#2',text='DURACIÓN')
        self.tabla.heading('#3',text='GENERO')
        
        for peli in self.lista_peliculas:
            self.tabla.insert(
                '',0,text=peli[0], values=(
                    peli[1],peli[2],peli[3]))
        self.boton_editar = tk.Button(self,text="Editar",command=self.editar_datos)
        self.boton_editar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
        self.boton_editar.grid(row=5,column=0,padx=10,pady=10)
        
        self.boton_eliminar = tk.Button(self,text="Eliminar",command=self.eliminar_datos)
        self.boton_eliminar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
        self.boton_eliminar.grid(row=5,column=1,padx=10,pady=10)
    
    def editar_datos(self):
        try:
            self.id_pelicula = self.tabla.item(self.tabla.selection())['text']
            self.nom_pelicula = self.tabla.item(self.tabla.selection())['values'][0]
            self.duracion_pelicula = self.tabla.item(self.tabla.selection())['values'][1]
            self.genero_pelicula = self.tabla.item(self.tabla.selection())['values'][2]
            self.habilitar_campos()
            self.input_nombre.insert(0,self.nom_pelicula)
            self.input_duracion.insert(0,self.duracion_pelicula)
            self.input_genero.insert(0,self.genero_pelicula)
            
        except Exception as error:
            print(error)
    
    def eliminar_datos(self):
        try:
            self.id_pelicula = self.tabla.item(self.tabla.selection())['text']
            self.pelicula.eliminar(self.id_pelicula)
            self.tabla_peliculas()
            self.id_pelicula=None
            
        except Exception as error:
            print(error)
