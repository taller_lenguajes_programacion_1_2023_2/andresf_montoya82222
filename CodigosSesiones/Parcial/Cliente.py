import Reserva as Reserva
class Cliente(Reserva):
    def __init__(self, id="", nombre="", apellido = "", dni=0, telefono="", direccion="", correoElectronico = ""):
        super().__init__()
        self.id = id
        self.nombre = nombre
        self.apellido = apellido
        self.dni = dni
        self.telefono = telefono
        self.direccion = direccion
        self.correoElectronico = correoElectronico

        # GETTERS
    def getId(self):
        return self.id

    def getNombre(self):
        return self.nombre

    def getApellido(self):
        return self.apellido

    def getDni(self):
        return self.dni
    
    def getTelefono(self):
        return self.telefono

    def getDireccion(self):
        return self.direccion

    def getCorreoElectronico(self):
        return self.correoElectronico

    # SETTERS

    def setId(self, id):
        self.id = id

    def setNombre(self, nombre):
        self.nombre = nombre

    def setApellido(self, apellido):
        self.apellido = apellido

    def setDni(self, dni):
        self.dni = dni

    def setTelefono(self, telefono):
        self.telefono = telefono

    def setDireccion(self, direccion):
        self.direccion = direccion

    def setCorreoElectronico(self, correoElectronico):
        self.correoElectronico = correoElectronico