#POO 
from estudio import Estudio
from habilidad import Habilidad
from laboral import Laboral
# la progracion poo tiene 4 pilares fundamentales : herencia , adstracion , polimorfismo y encapsulamiento

# clases atributos y funciones.


class Persona:
    def __init__(self,id = 0,nom1="",nom2="",apell1="",apell2="",f_nac="",tel = 0,cel = 0,email="",descrip=""):
        #atributos
        self.__id = id
        self.__nom1=nom1
        self.__nom2=nom2
        self.__apell1=apell1
        self.__apell2=apell2
        self.__f_nac=f_nac
        self.__tel = tel
        self.__cel = cel
        self.__email=email
        self.__descrip=descrip
        self.__xlsx = Excel("hoja_vida.xlsx")
        self.__df_per = self.__xlsx.leer_xlsx("personas")
        self.__estudios = Estudio()
        self.__habilidades = Habilidad()
        self.__laboral = Laboral()


    @property
    def _id(self):
        return self.__id

    @_id.setter
    def _id(self, id=0):
        self.__id = id


    @property
    def _nom1(self):
        return self.__nom1

    @_nom1.setter
    def _nom1(self, value):
        self.__nom1 = value

    @property
    def _nom2(self):
        return self.__nom2

    @_nom2.setter
    def _nom2(self, value):
        self.__nom2 = value

    @property
    def _apell1(self):
        return self.__apell1

    @_apell1.setter
    def _apell1(self, value):
        self.__apell1 = value

    @property
    def _apell2(self):
        return self.__apell2

    @_apell2.setter
    def _apell2(self, value):
        self.__apell2 = value

    @property
    def _f_nac(self):
        return self.__f_nac

    @_f_nac.setter
    def _f_nac(self, value):
        self.__f_nac = value

    @property
    def _tel(self):
        return self.__tel

    @_tel.setter
    def _tel(self, value):
        self.__tel = value

    @property
    def _cel(self):
        return self.__cel

    @_cel.setter
    def _cel(self, value):
        self.__cel = value

    @property
    def _email(self):
        return self.__email

    @_email.setter
    def _email(self, value):
        self.__email = value

    @property
    def _descrip(self):
        return self.__descrip

    @_descrip.setter
    def _descrip(self, value):
        self.__descrip = value

    @property
    def _estudios(self):
        return self.__estudios

    @_estudios.setter
    def _estudios(self, value):
        self.__estudios = value

    @property
    def _habilidades(self):
        return self.__habilidades

    @_habilidades.setter
    def _habilidades(self, value):
        self.__habilidades = value

    @property
    def _laboral(self):
        return self.__laboral

    @_laboral.setter
    def _laboral(self, value):
        self.__laboral = value

    
    def __str__(self):
        return f"{self.__id} {self.__nom1} {self.__estudios} {self.__habilidades} {self.__laboral}"
        
