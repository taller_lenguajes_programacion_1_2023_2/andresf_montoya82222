"""
URL configuration for AppPortal project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Web.views import index, login, registro
from ProductosCafe.views import visualizarProductos, regProductos, compras

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name="index"),
    path('login/', login, name="Login"),
    path('registro/', registro, name="registro"),
    path('regProductos/', regProductos, name="regProductos"),
    path('visualizar/', visualizarProductos, name='visualizar'),
    path('compras/<int:producto_id>/', compras, name='compras'),
]
