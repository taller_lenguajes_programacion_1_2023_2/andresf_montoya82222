## 1. Imprimir HOLA MUNDO ##


print("Hola Mundo")

## 2. Variables y asignacion ##

Variable = "Andres"
print(Variable)
Variable2 = 10
print(Variable2)
Variable3 = 0.1
print(Variable3)

## 3. Tipos de variables: int, String ##
print(type(Variable))
print(type(Variable2))
print(type(Variable3))

## 4. Operaciones aritmeticas basicas ##
suma = 3+5
resta = 5-4
multi = 5*6
division = 6/2
potencia = 2**2

## 5. Conversion entre tipos de datos ##
variablecambio = str(Variable2)

## 6. Solicitar entrada al usuario
variableEscriba = int(input("Ingresa su edad"))
variableString = str(input("Ingrese el nombre"))

## 7,8 y 9. Condicional if, if-else, if-elif-else ##
if variableEscriba > 0:
    print("Niño")
elif variableEscriba < 18:
    print("Adolecente")
else:
    print("Adulto")

## 10. Bucle for ##
lista1=[1,4,8,29,10]
for num in lista1:
    print(num)
for num in range(0,101):
    print(num)

## 11. Bucle While ##
while num<8:
    num = num+1
    print(num)

## 12. Uso de break y continue ##
while num<8:
    num = num+1
    break
while num<8:
    if num == 7:
        continue
    else:
        break
    num = num+1

## 13, 14, 15, 16 y 17. Listas y sus operaciones basicas ##
listasPrincipal = ("Mario", "Jose", "Pedro", "Juan", "Andrés", 6) #Tupla
listasPrincipal.count("Pedro")
print(type(listasPrincipal))
print(listasPrincipal[0])
print(listasPrincipal.count)
listasPrincipal2 = [1,2,3,5,6,9,100] #Lista
listasPrincipal2.append(200)
listasPrincipal2.remove(9)
listasPrincipal2.insert(9,101)
listasPrincipal2.copy()
listasPrincipal2.pop(1)
print(listasPrincipal2)
listasPrincipal2.clear()


listasPrincipal3 = {
    "Nombre" : "Andres",
    "Edad" : 24,
    "Genero": 'H'
} #Diccionario
listasPrincipal4 = {"Clase", "Algoritmos", "Taller", 1}#Conjunto
print(len(listasPrincipal4))
## 20. Modos de apertura: r, w, a, rb, wb ##
    ## 18. Leer un archivo de texto ##
with open (".\CodigosSesiones\ArchivosPlanos\Clase6(Repaso)\ArchivoRepaso.txt", "r") as prueba:
        print(prueba.read())
    ## 19. Escribir en un archivo de texto ##
with open (".\CodigosSesiones\ArchivosPlanos\Clase6(Repaso)\ArchivoRepaso.txt", "w") as prueba:
        print(prueba.write("Puedes escribir lo que quieras"))
    ##
## 21. Trabajar con archivos JSON, Crear un dataframe ##

datos = {
     "Nombre" : ["Andres", "Jose"],
    "Edad" : [24,42],
    "Genero": ['H','H']
}
import json
with open (".\CodigosSesiones\ArchivosPlanos\Clase6(Repaso)\Repaso.json", "w") as js:
        json.dump(datos,js)

## 22. Leer un archivo CSV ##
import csv
with open (".\CodigosSesiones\ArchivosPlanos\Clase6(Repaso)\RepasoCSV.csv", "r") as csvArchivo:
        csvString=csvArchivo.read()
## 23. Filtrar datos en un dataframe ##
import pandas as pd

datosImportantes = {
    "Nombre": "Marco",
    "Edad": 26,
    "Profesión": "Diseñador",
    "Teléfono": 3102940124
}

df =  pd.read_excel(r"./CodigosSesiones/ArchivosPlanos/Clase6(Repaso)/clase_repaso_dataset.xlsx")
print(df.columns) #imprime solo las primeras columnas
print(df.dtypes) #tipos de columnas (object, int64)
print(df.iloc[1]) #mostrar una fila dependiendo el numero
print(df["Nombre"]) #mostrar datos de una columna dada
print(df["Nombre"].iloc[1]) #mostrar datos de una columna dada en columna especifica

## 24. Operaciones básicas: sum(), mean(), max(), min()
import numpy as np
listaDePares = [i for i in range(0,101) if i%2==0]
print(listaDePares)
print(np.sum(listaDePares))
print(np.mean(listaDePares))
print(np.max(listaDePares))
print(np.min(listaDePares))

## 25. Uso de Iloc y loc ##
df2 = pd.read_excel(r"./CodigosSesiones/ArchivosPlanos/Clase6(Repaso)/clase_repaso_dataset.xlsx")
df2.loc[1,3]
df2.iloc[2,6]

## 26. Agrupar datos con grouphy ##
df3 = pd.DataFrame({
    "nombre": ["Ana", "Mateo", "Jairo", "Andrés", "Alejandro", "Jose"],
    "edad": [21, 22, 21, 21, 19, 21] 
})

df3.groupby("nombre")
for name in df3.groupby("nombre"):
     print(name)

## 27. Unir DataFrames con merge y concat ##

df4 = pd.DataFrame({
     "A": [1,5,6,8,10],
     "B": [5,1,6,8,10,9]
})
df5 = pd.DataFrame({
     "A": [1,3,11,4,10],
     "B": [7,2,6,8,10,4],
     "C": [21,3,2,12,24,47]
})

concat = pd.concat([df4,df5], axis=1)
print(f"Concat:\n{concat}")

merge = pd.merge(df4,df5, on="A")
print(f"Merge:\n{merge}")

## 28. Manipular series temporales ##

Numeros = (2,6,8,10,24,12)
serial = pd.Series(Numeros)

print(serial)

## 29. Exportar un dataframe a CSV ##

df6 = pd.DataFrame({
    "nombre": ["Ana", "Mateo", "Jairo", "Andrés", "Alejandro", "Jose"],
    "edad": [21, 22, 21, 21, 19, 21] 
})

print(df6)
df6.to_csv("./CodigosSesiones/ArchivosPlanos/Clase6(Repaso)/Clase6v1.csv")

## 30. Convertir un dataframe a Json ##

df6 = pd.DataFrame({
    "nombre": ["Ana", "Mateo", "Jairo", "Andrés", "Alejandro", "Jose"],
    "edad": [21, 22, 21, 21, 19, 21] 
})

df6.to_json("./CodigosSesiones/ArchivosPlanos/Clase6(Repaso)/Clase6v2.json")
print(df6.to_json())