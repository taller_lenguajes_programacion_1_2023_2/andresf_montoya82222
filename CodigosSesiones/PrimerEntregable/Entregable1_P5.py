## Entregable #1- Andrés Felipe Montoya López- 1000758778 ##

## 48. Escribe un programa que combine dos archivos CSV en uno solo ##
import pandas as pd
afml_datos1 = pd.read_csv(".\CodigosSesiones\PrimerEntregable\Archivos\P5_Archivo1.csv")
afml_datos2 = pd.read_csv(".\CodigosSesiones\PrimerEntregable\Archivos\P5_Archivo2.csv")

afml_combinarcsv = afml_datos1 + afml_datos2

afml_combinarcsv.to_csv(".\CodigosSesiones\PrimerEntregable\Archivos\P5_ArchivoDoble.csv")
