from django.shortcuts import render, redirect
from .models import Usuario
# Create your views here.



# Create your views here.
def index(request):
    return render(request, "index.html")

def login(request):
    mensajeError = ""
    if request.method == 'POST':
        correo = request.POST['Correo']
        clave = request.POST['Clave']
        try:
            usuarioEn = Usuario.objects.get(correoUsuario = correo)
            if usuarioEn.claveUsuario == clave:
                return redirect('regProductos')
            else:
                mensajeError = "Contrasena incorrecta"
        except:
            mensajeError = "Usuario no encontrado"
    return render(request, "login.html",{
        "error":mensajeError
    })

def registro(request):
    mensajeError = "No hay datos correctos"
    if request.method == 'POST':
        nombre = request.POST['Nombre']
        apellido = request.POST['Apellido']
        correo = request.POST['Correo']
        clave = request.POST['Clave']

        try:
            usuarioEn = Usuario.objects.get(correoUsuario = correo)
            mensajeError = "Usuario ya se encuentra registrado"
        except:
            nuevoUser = Usuario.objects.create(nombreUsuario = nombre, apellidoUsuario = apellido, 
            correoUsuario = correo, claveUsuario = clave)
            return redirect('Login')



    return render(request, "registro.html",{
        "error": mensajeError
    })
