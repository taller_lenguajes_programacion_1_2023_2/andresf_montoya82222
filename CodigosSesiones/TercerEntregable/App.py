import tkinter as tk
import sqlite3

from Club import Club

class App(Ventana):

    def __init__(self):
        super().__init__()
        self.title("Clubes")
        self.geometry("500x300")
        self.label_titulo = tk.Label(self, text="Lista de clubes")
        self.label_titulo.pack()

        self.lista_clubes = tk.Listbox(self)
        self.lista_clubes.pack()

        self.boton = tk.Button(self, text="Obtener", command=self.on_click)
        self.boton.pack()

    def on_click(self):
        conn = sqlite3.connect("database.sqlite3")
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM clubs")
        clubes = cursor.fetchall()
        conn.close()
        self.lista_clubes.delete(0, tk.END)
        for club in clubes:
            self.lista_clubes.insert(tk.END, club[1])

    def obtener_club(self, id):
        conn = sqlite3.connect("database.sqlite3")
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM clubs WHERE id = ?", (id,))
        club = cursor.fetchone()
        conn.close()
        return Club(*club)

if __name__ == "__main__":
    ventana = App()
    ventana.mainloop()