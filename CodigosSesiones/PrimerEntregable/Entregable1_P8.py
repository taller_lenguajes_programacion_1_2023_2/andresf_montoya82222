## Entregable #1- Andrés Felipe Montoya López- 1000758778 ##

## 78. Simula un archivo Excel, escribe un programa que muestre un resumen estadistico en una columna ##

import pandas as pd
afml_datos = pd.read_excel(".\CodigosSesiones\PrimerEntregable\Archivos\P7_alumnos.xlsx")
afml_DataFrame1 = pd.read_excel(".\CodigosSesiones\PrimerEntregable\Archivos\P7_alumnos.xlsx", sheet_name="Hoja1", header=None, names=['Nombre', 'Asignatura', 'Promedio'])
afml_estadistico = afml_DataFrame1.describe()

print(afml_estadistico)