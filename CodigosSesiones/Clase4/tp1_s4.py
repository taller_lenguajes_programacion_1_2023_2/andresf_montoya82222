# *********** Dataframe con pandas **********

# pandas estructuras bidimencionales

datos = {
    "Nombre": ["Andrés"],
    "Edad": [21],
    "Profesión": ["Estudiante de Ingenieria informatica"],
    "Teléfono": [3234108779]
}
datos_4 = {
    "Nombre": "Andrés",
    "Edad": 21,
    "Profesión": "Estudiante de Ingenieria informatica",
    "Teléfono": 3234108779
}

datos_2=["Andrés",21,"Estudiante de Ingenieria informatica",3234108779]
#datos_3=[datos["Nombre"], datos["Edad"],datos["Profesión"], datos["Teléfono"]]

# Importar libreria

import pandas as pd

#df = pd.DataFrame(data=datos)
#print(df)

df = pd.read_excel(r"./CodigosSesiones/ArchivosPlanos/Clase4/clase_4_dataset.xlsx")
print(df)
# print(df.columns) imprime solo las primeras columnas
# print(df.dtypes) tipos de columnas (object, int64)
# print(df.iloc[1]) mostrar una fila dependiendo el numero
# print(df["Nombre"]) mostrar datos de una columna dada
# print(df["Nombre"].iloc[1]) mostrar datos de una columna dada en columna especifica

#fila vacia
#df["Fecha"]=""

#df.loc[len(df)] = datos_2 Lista tupla
df.loc[len(df)] = datos_4 
print(df)
#print(df.dtypes)
df.at[2,"Profesión"] = "Contadora"
print(df.at[2,"Profesión"])
#df.loc[len(df)] = datos
# df.loc[7] = datos

#Recorrer un dataframe

for index,row in df.iterrows():
    print("la fila {}, columna {}". format(index,row["Profesión"]))