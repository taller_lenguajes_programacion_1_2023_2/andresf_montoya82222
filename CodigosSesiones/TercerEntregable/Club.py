import sqlite3

class Club:

    def __init__(self, id, nombre_club, fecha_fundacion):
        self.id = id
        self.nombre_club = nombre_club
        self.fecha_fundacion = fecha_fundacion

    def __str__(self):
        return f'Club {self.id}: {self.nombre_club}'

    def guardar(self):
        conn = sqlite3.connect('database.sqlite3')
        cur = conn.cursor()
        cur.execute('''INSERT INTO clubs (name, foundation_date) VALUES (?, ?)''', (self.nombre_club, self.fecha_fundacion))
        conn.commit()
        conn.close()

    def obtener_por_id(id):
        conn = sqlite3.connect('database.sqlite3')
        cur = conn.cursor()
        cur.execute('''SELECT * FROM clubs WHERE id = ?''', (id,))
        club = cur.fetchone()
        conn.close()
        return Club(*club) if club is not None else None

    def actualizar(self):
        conn = sqlite3.connect('database.sqlite3')
        cur = conn.cursor()
        cur.execute('''UPDATE clubs SET name = ?, foundation_date = ? WHERE id = ?''', (self.nombre_club, self.fecha_fundacion, self.id))
        conn.commit()
        conn.close()

    def eliminar(self):
        conn = sqlite3.connect('database.sqlite3')
        cur = conn.cursor()
        cur.execute('''DELETE FROM clubs WHERE id = ?''', (self.id,))
        conn.commit()
        conn.close()

