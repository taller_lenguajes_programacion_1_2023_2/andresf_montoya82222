import Reserva as Reserva
class Hotel(Reserva):
    def __init__(self, id="", nombre="", direccion = "", estrellas=0, servicios=[""], numeroHabitaciones=0, dueño = ""):
        super().__init__()
        self.id = id
        self.nombre = nombre
        self.direccion = direccion
        self.estrellas = estrellas
        self.servicios = servicios
        self.numeroHabitaciones = numeroHabitaciones
        self.dueño = dueño

        super().__init__(id, )
        # GETTERS
    def getId(self):
        return self.id

    def getNombre(self):
        return self.nombre

    def getDireccion(self):
        return self.direccion

    def getEstrellas(self):
        return self.estrellas
    
    def getServicios(self):
        return self.servicios

    def getNumeroHabitaciones(self):
        return self.numeroHabitaciones

    def getDueño(self):
        return self.dueño

    # SETTERS

    def setId(self, id):
        self.id = id

    def setNombre(self, nombre):
        self.nombre = nombre

    def setDireccion(self, direccion):
        self.direccion = direccion

    def setEstrellas(self, estrellas):
        self.estrellas = estrellas

    def setServicios(self, servicios):
        self.servicios = servicios

    def setNumeroHabitaciones(self, numeroHabitaciones):
        self.numeroHabitaciones = numeroHabitaciones

    def setDueño(self, dueño):
        self.dueño = dueño


