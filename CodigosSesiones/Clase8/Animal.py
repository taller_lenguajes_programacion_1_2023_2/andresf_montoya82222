#herencia

class Animal:
    def __init__(self) -> None:
        pass
    
    def caminar(self,texto=""):
        print("{} camino".format(texto))
    
    def correr(self,texto):
        print("{} corro".format(texto))
    
    def __str__(self) -> str:
        return "Soy un animal"
    

class Gato(Animal):
    def __init__(self) -> None:
        self.gato = "soy un gato"
        super().__init__()
        
    def caminar(self):
        return super().caminar(self.gato)
    
    def correr(self):
        return super().correr(self.gato)
    
    def __str__(self) -> str:
        return super().__str__()
    
class Perro(Animal):
    def __init__(self) -> None:
        self.perro = "soy un perro"
        self.patas = " en 4 patas"
        super().__init__()
    
    def caminar(self,texto=""):
        texto = "{} y camino {} {}".format(self.perro, self.patas,texto)
        return super().caminar(texto)
    
    def ladra(self):
        print("guuuaguaa")
    
    def correr(self):
        return super().correr(self.perro)
    
    def __str__(self) -> str:
        return super().__str__()

class Labrador(Perro):
    def __init__(self) -> None:
        super().__init__()
    
    def caminar(self):
        texto = super().caminar(" , ademas soy labrador")
        return texto
    
    def correr(self):
        return super().correr()
    
perro = Perro()
gato = Gato()
labrador = Labrador()
perro.caminar()
gato.caminar()
