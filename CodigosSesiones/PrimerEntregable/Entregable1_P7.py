## Entregable #1- Andrés Felipe Montoya López- 1000758778 ##

## 68. Simule un archivo Excel alumnos.xlsx, escribe un programa que ordene los datos por promedio del alumno ##

import pandas as pd

afml_datos = pd.read_excel(".\CodigosSesiones\PrimerEntregable\Archivos\P7_alumnos.xlsx")

afml_ordenardatos = afml_datos.sort_values('Promedio')

afml_ordenardatos.to_excel(".\CodigosSesiones\PrimerEntregable\Archivos\P7_alumnosOrden.xlsx")


