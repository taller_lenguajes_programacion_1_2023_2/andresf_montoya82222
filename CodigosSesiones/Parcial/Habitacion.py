import conexion as base
class Habitacion(base):
    def __init__(self, id="", numero = 0, tipo = "", capacidad = 0, precioNoche = 0.0, amenities = [""], vista=""):
        super().__init__()
        self.id = id
        self.numero = numero
        self.tipo = tipo
        self.capacidad = capacidad
        self.precioNoche = precioNoche
        self.amenities = amenities
        self.vista = vista

        # GETTERS
    def getId(self):
        return self.id

    def getNumero(self):
        return self.numero

    def getTipo(self):
        return self.tipo

    def getCapacidad(self):
        return self.capacidad
    
    def getPrecioNoche(self):
        return self.precioNoche

    def getAmenities(self):
        return self.amenities

    def getVista(self):
        return self.vista

    # SETTERS

    def setId(self, id):
        self.id = id

    def setNumero(self, numero):
        self.numero = numero

    def setTipo(self, tipo):
        self.tipo = tipo

    def setCapacidad(self, capacidad):
        self.capacidad = capacidad

    def setPrecioNoche(self, precioNoche):
        self.precioNoche = precioNoche

    def setAmenities(self, amenities):
        self.amenities = amenities

    def setVista(self, vista):
        self.vista = vista
